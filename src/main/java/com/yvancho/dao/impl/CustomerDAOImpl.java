package com.yvancho.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.yvancho.beans.Customer;
import com.yvancho.dao.CustomerDAO;

public class CustomerDAOImpl implements CustomerDAO {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void insert(Customer customer) {

		String sql = "INSERT INTO CUSTOMER "
				+ "(CUST_ID, NOMBRE, EDAD, DIRECCION) VALUES (?, ?, ?, ?)";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, customer.getCustId());
			ps.setString(2, customer.getNombre());
			ps.setInt(3, customer.getEdad());
			ps.setString(4, customer.getDireccion());

			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public Customer findByCustomerId(int custId) {

		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";

		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, custId);
			
			Customer customer = null;
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				customer = new Customer(rs.getInt("CUST_ID"),
						rs.getString("NOMBRE"), rs.getInt("EDAD"),
						rs.getString("DIRECCION"));
			}
			rs.close();
			ps.close();

			return customer;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

}
