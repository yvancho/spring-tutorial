package com.yvancho.dao;

import com.yvancho.beans.Customer;

public interface CustomerDAO {

	public void insert(Customer customer);

	public Customer findByCustomerId(int custId);
}
