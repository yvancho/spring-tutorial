package com.yvancho.marshal;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

public class XMLConverter {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;

	public Marshaller getMarshaller() {
		return marshaller;
	}

	public void setMarshaller(Marshaller marshaller) {
		this.marshaller = marshaller;
	}

	public Unmarshaller getUnmarshaller() {
		return unmarshaller;
	}

	public void setUnmarshaller(Unmarshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}

	// metodos convertidores

	public void convertFromObjectToXML(Object object, String filePath)
			throws IOException {

		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(filePath);
			getMarshaller().marshal(object, new StreamResult(fos));
		} finally {
			if (fos != null) {
				fos.close();
			}
		}

	}

	public Object convertFromXMLToObject(String xmlFile) throws IOException {

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(xmlFile);
			return getUnmarshaller().unmarshal(new StreamSource(fis));
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
	}

}
