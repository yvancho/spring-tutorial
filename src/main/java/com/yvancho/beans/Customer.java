package com.yvancho.beans;

public class Customer {

	private int custId;
	private String nombre;
	private int edad;
	private String direccion;

	public Customer(int custId, String nombre, int edad, String direccion) {
		super();
		this.custId = custId;
		this.nombre = nombre;
		this.edad = edad;
		this.direccion = direccion;
	}

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", nombre=" + nombre + ", edad="
				+ edad + ", direccion=" + direccion + "]";
	}

}
