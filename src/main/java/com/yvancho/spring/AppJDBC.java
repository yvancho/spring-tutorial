package com.yvancho.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yvancho.beans.Customer;
import com.yvancho.dao.CustomerDAO;

public class AppJDBC {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/yvancho/xml/Spring-Module.xml");

		CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
		
		Customer customer = new Customer(1, "mkyong", 28, "Jr Lima 123");
		customerDAO.insert(customer);

		Customer customer1 = customerDAO.findByCustomerId(1);
		System.out.println(customer1);
	}

}
