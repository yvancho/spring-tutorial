package com.yvancho.spring;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yvancho.beans.Customer;
import com.yvancho.marshal.XMLConverter;

public class AppConvert {

	private static final String XML_FILE_NAME = "customer.xml";

	public static void main(String[] args) throws IOException {

		ApplicationContext appContext = new ClassPathXmlApplicationContext("com/yvancho/xml/beans.xml");		
		XMLConverter converter = (XMLConverter) appContext.getBean("XMLConverter");
		
		Customer customer = new Customer();
		customer.setCustId(99);
		customer.setNombre("yvan");
		customer.setEdad(32);	
		customer.setDireccion("This is address");
		
		//from object to XML file
		System.out.println("Convert Object to XML!");		
		converter.convertFromObjectToXML(customer, XML_FILE_NAME);
		System.out.println("Done \n");
		
		//from XML to object
		System.out.println("Convert XML back to Object!");		
		Customer customer2 = (Customer)converter.convertFromXMLToObject(XML_FILE_NAME);
		System.out.println(customer2);
		System.out.println("Done");	

	}

}
