package com.yvancho.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.yvancho.beans.AppConfig;
import com.yvancho.beans.Mundo;

public class App {

	public static void main(String[] args) {

//		ApplicationContext appContext = new ClassPathXmlApplicationContext(
//				"com/yvancho/xml/beans.xml");
		
		ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Mundo m = (Mundo) appContext.getBean("mundo");

		System.out.println(m.getSaludo());

		((ConfigurableApplicationContext) appContext).close();

	}

}
